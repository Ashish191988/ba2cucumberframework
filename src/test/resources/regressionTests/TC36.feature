Feature: verify search SOE functionality from HR portal

@RegressionTestttt
Scenario: verify search SOE functionality from HR portal
Given user login into EE application
And user click enter button under admin section
When user click start enrollment tab
And user enter "employee_id" in enrolment search text box
And user click search button to search SOE
Then verify employees SOE header as employee
