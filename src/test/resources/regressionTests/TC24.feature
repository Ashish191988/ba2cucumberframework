Feature: verify Adjudicate functionality for claim type as Medical Expenses (FSA) claim for  BA2 MA application 

@RegressionTestttt
Scenario Outline: verify Adjudicate  functionality for claim type as Medical Expenses (FSA) claim for  BA2 MA site
Given user login into MA application
When user click company link
And user click claims link
And user click search a claim link
And user enter value for "<transaction_code>" text box
Then user click search button
And user click adjudicate button
Then user click claim status as "<Status>" from dropdown
And user click process button
And user click on OK pop up button
Then verify "<Status Text>" successfully has been displayed

|transaction_code|Status|Status Text|
|transaction_code|Approve|Status Text|
|transaction_code|Reject|Status Text|
|transaction_code|Void|Status Text|