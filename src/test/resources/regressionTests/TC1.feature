Feature: Verify employee and dependent profile upload functionality for BA2 application

@RegressionTesttttt
Scenario: Verify employee CSV upload functionality
Given user launched the browser for automation
When user enter the email address in the email address field
Then user click on the next button
And user click on the upload employee data button
Then user upload the employee csv 
And user click on the start upload button
Then user click on the submit button
And user click on the close button
And user click on the upload Dependent data button
Then user upload the dependent csv 
And user click on the dep start upload button
Then user click on the dep submit button
And user click on the close button
Then user click on the Initialization link under implementation  
And user enter enrolment from date in the field
And user enter enrolment to date in the field
And user enter the username in the employee field
Then user click on the search empolyee button
And user enter the employee name in the search field
Then user select employee radio button
And user click on the search button
Then user select no of records from the dropdown
Then user click on the intilization submit button
And click on the ok button
Then user click on the close button of job progress pop up
And user click on the logout button














