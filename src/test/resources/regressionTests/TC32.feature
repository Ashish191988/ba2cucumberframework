Feature: Verify Claim Listing Report functionality from MA portal

@RegressionTestttt
Scenario: Verify Claim Listing Report functionality from MA portal
Given user login into MA application
When user mouse over reports and Statistics tab
And user mousehover Claims Reports tab
And user click on the Claim Listing Report
And user enter "Employee Id" in Employee ID  
And user click search button
Then verify report header as "Claim Listing Report"