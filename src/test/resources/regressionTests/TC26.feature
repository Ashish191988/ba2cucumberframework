Feature: Verify claim review functionality in MA application

@RegressionTestttt
Scenario Outline: Verify claim review functionality for MA site
Given user login into MA application
When user click company link
And user click claims link
And user click search a claim link
And user enter value for "<transaction_code>" text box
Then user click search button
Then verify that same "<transaction_code>" record is displayed on screen
And user click review button
And user click review all button
And user click on OK button on review all screen pop up
And user click review all link
And user click on ok button reviewed pop up
And user click on start button
Then verify mass review completed successfully is displayed on screen. 
And user click on close button 

|transaction_code|

