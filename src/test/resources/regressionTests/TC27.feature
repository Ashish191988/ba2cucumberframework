Feature: Verify payment functionality for claim

@RegressionTestttt
Scenario Outline: Verify payment functionality from MA application
Given user login into MA application
When user mose over payment link
And user select generate files (New) link 
And user enter salary deduction date "<start date>" and "<end date>"
|start date|end date|
|          |        |
Then user select generate button
And verify payment generated pop up window is displayed
And user select close button on pop up screen
And verify amount generated as "number"
Then user select check box for payment
And user click confirm button
And user click ok button for payment
And verify payment confirmation is dislayed on popup screen
And user select close button on confirmation popup screen
