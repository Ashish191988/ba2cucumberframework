Feature: Verify claim add to batch functionality for claim type as Dental expenses claim in BA2 application

@RegressionTestttt
Scenario Outline: Verify claim add to batch functionality for claim type as Dental expenses claim for MA site
Given user login into MA application
When user click company link
And user click claims link
And user click search a claim link
And user enter value for "<transaction_code>" text box
Then user click search button
And user click add to batch button
And user click new batch button
Then user enter value for "<batch_name>" text box
And user click save button
And user click OK pop up button 
And user click add document to batch link
And user enter value for "<transaction_code>" text box
Then user click add button

|transaction_code|batch_name |
|Value_1         |batch_test1| 
|Value_2         |batch_test1| 
|Value_3         |batch_test1| 