Feature: Verify Panel Claim Listing Report functionality from MA portal

@RegressionTestttt
Scenario: Verify Panel Claim Listing Report functionality from MA portal
Given user login into the application
When user click on the Panel Claim Listing Report under Report Stat
And user click Generate button
Then verify report header as Panel Claim Listing Report
