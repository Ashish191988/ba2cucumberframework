Feature: verify Employee profile report functionality from HR portal

@RegressionTestttt
Scenario: verify Employee profile report details functionality from HR portal
Given user login into EE application
And user click enter button under admin section
When user click reports tab
And user click employee profile report link
And user click employee status as "Active"
And user click employee search button
Then verify employee report header as "Employee Profile Report"