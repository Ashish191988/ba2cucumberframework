Feature: verify Flex Balance & Usage Report
 report functionality from HR portal

@RegressionTestttt
Scenario: verify Flex Balance & Usage Report
 report details functionality from HR portal
Given user login into EE application
And user click enter button under admin section
When user click reports tab
And user click dsosg flex point detailed report link
And user click employee flex status as "Active"
And user click flex report search button
Then verify report header as "Flex Balance & Usage Report"