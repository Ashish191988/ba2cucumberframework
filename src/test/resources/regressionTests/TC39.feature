Feature: verify Dependent profile report functionality from HR portal

@RegressionTestttt
Scenario: verify Dependent profile report details functionality from HR portal
Given user login into EE application
And user click enter button under admin section
When user click reports tab
And user click dependent profile report link
And user click dependent status as "Active"
And user click dependent search button
Then verify  dependent report header as "Dependent Profile Report"
