Feature: Verify Panel claim upload functionality for BA2 application

@RegressionTestttt
Scenario: Verify panel claim CSV upload functionality
Given user launched the browser for automation
When user enter the email address in the email address field
Then user click on the next button
And user click on the panel claims Upload (New) button
Then user upload the panel claim csv 
And user click on the start upload button
Then user click on the submit button
And user click on the close button

