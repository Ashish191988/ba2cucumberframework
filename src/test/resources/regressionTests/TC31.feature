Feature: Verify FLEX POINTS DETAILED Report functionality from MA portal

@RegressionTestttt
Scenario: Verify FLEX POINTS DETAILED Report functionality from MA portal
Given user login into MA application
When user mouse over reports and Statistics tab
And user mousehover DSOSG tab
And user click on the DSOSG FLEX POINTS DETAILED REPORT
And user enter "Employee Id" in Employee Listing  
And user click search button
Then verify report header as "FLEX POINTS DETAILED REPORT"

