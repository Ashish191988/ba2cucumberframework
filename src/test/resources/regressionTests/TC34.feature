Feature: Verify user is able to add employee on HR module

@RegressionTesttttt
Scenario: Verify user has added employee on HR module
Given user login into the application
When user click on security access link and click on manage user link
And user click on add client admin button
And user click on search for employee button 
And user enter employee name as "BA2_DSO_037" and click on search button
And user click on employee name link
Then user click on save button 