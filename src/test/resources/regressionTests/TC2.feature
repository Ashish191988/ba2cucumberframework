Feature: Verify employee and dependent profile upload functionality for BA2 application

@RegressionTest
Scenario: Verify employee CSV upload functionality
Given user login into the application
Then user upload the employee data
Then user upload the dependent data
And user intilized the uploaded employee
Then user logout from the application