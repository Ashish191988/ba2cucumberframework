Feature: verify search claim functionality from EE portal

@RegressionTestttt
Scenario Outline: Verify search claim functionality for claim type as Medical Expenses (FSA) claim for  BA2 EE site
Given user login into EE application
When user should select search claim link
And user enter "<claimant_name>" on search claim screen 
Then user retrived "<Transaction_Code>" 

|transaction Code|claimant_name  |
|                |Employee       |
|                |Employee_Spouse|
|                |Employee_Child |
