Feature: Verify claimsubmission  functionality for claim type as Dental expenses claim in BA2 application

@RegressionTestttt
Scenario Outline: Verify claim submit functionality for claim type as Dental expenses claim from EE site
Given user login into EE application
When user select create new claim link
And user select "<claimant_name>" dropdown on EE claim screen
And user select claim type as "   Dental expenses (Not Claimable from MSA)" from drop-down list
And user enter value for "<clinic_provider_name>" text box
And user enter value for "<receipt_date>" text box
And user enter value for "<receipt_no>" text box
And user enter value for "<receipt_amount>" text box
And user enter value for "<remark>" text box
And user upload document through attach scanned receipt link
And user select ready to submit button 
And user select confirm claims(s) button
And user select ok button
Then verify search claim screen is displayed
And user retrive tranction code for first claim


|claimant_name|clinic_provider_name|receipt_date |receipt_no|receipt_amount|remark|
|Employee     |clinic91            |01_April_2020|Receipt01 |245           |Test1 |
|Spouse       |clinic92            |02_April_2020|Receipt02 |60            |Test2 |
|Child        |clinic93            |03_April_2020|Receipt03 |160           |Test3 |


