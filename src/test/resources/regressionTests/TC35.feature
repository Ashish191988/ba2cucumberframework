Feature: Verify Search Employee functionality from HR portal

@RegressionTesttttt
Scenario: Verify search Employee and dependent details functionality from HR portal
Given user login into EE application
And user click enter button under admin section
When user click search employee tab
And user select employee id radio button
And user enter "employee_id" in search text box
And user click search button to search employee
And user click employee link
Then verify header for Employee Detail page      
When user click dependent link
Then verify verify header for Dependent detail page
When user click stataement of account link
Then verify header for stetement of account page


