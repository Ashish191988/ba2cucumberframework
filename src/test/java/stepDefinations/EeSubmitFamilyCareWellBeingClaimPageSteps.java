package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.EeSubmitClaimCommonPage;


@SuppressWarnings("deprecation")
public class EeSubmitFamilyCareWellBeingClaimPageSteps {
	
	EeSubmitClaimCommonPage eeSubmitClaimCommonPage;
	TestContext testContext;
	

	public EeSubmitFamilyCareWellBeingClaimPageSteps(TestContext context) {
		testContext = context;
		eeSubmitClaimCommonPage = testContext.getPageObjectManager().getEeSubmitClaimCommonPage();
	}
	
	@When("user select create new claim link")
	public void user_select_create_new_claim_link() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user select {string} dropdown on EE claim screen")
	public void user_select_dropdown_on_EE_claim_screen(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user select claim type as {string} from drop-down list")
	public void user_select_claim_type_as_from_drop_down_list(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user enter value for {string} text box")
	public void user_enter_value_for_text_box(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user upload document through attach scanned receipt link")
	public void user_upload_document_through_attach_scanned_receipt_link() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user select ready to submit button")
	public void user_select_ready_to_submit_button() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user select confirm claims\\(s) button")
	public void user_select_confirm_claims_s_button() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("user select ok button")
	public void user_select_ok_button() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Then("verify search claim screen is displayed")
	public void verify_search_claim_screen_is_displayed(io.cucumber.datatable.DataTable dataTable) {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
	    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
	    // Double, Byte, Short, Long, BigInteger or BigDecimal.
	    //
	    // For other transformations you can register a DataTableType.
	    throw new cucumber.api.PendingException();
	}

	@Then("user retrive tranction code for first claim")
	public void user_retrive_tranction_code_for_first_claim(io.cucumber.datatable.DataTable dataTable) {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
	    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
	    // Double, Byte, Short, Long, BigInteger or BigDecimal.
	    //
	    // For other transformations you can register a DataTableType.
	    throw new cucumber.api.PendingException();
	}


}

