package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MaEmployeeProfileReportPage;

public class MaEmployeeProfileReportSteps {

	
	
	MaEmployeeProfileReportPage maEmployeeProfileReportPage;
	TestContext testContext;
	

	public MaEmployeeProfileReportSteps(TestContext context) {
		testContext = context;
		maEmployeeProfileReportPage = testContext.getPageObjectManager().getMaReportsPage();
	}
	
	



	@When("user click on the Employee profile report under Report Stat")
	public void user_click_on_the_Employee_profile_report_under_Report_Stat() {
		maEmployeeProfileReportPage.clickEmployeeProfileReport();
	}

	//@When("user select employee status as Active")
	//public void user_select_employee_status_as_Active() {
	//	maEmployeeProfileReportPage.clickEmployeeStatusDd();
//	}

	/*@When("user click employee search button")
	public void user_click_employee_search_button() {
		maEmployeeProfileReportPage.clickEmployeeSearchButton();
	}*/

	@Then("verify report header as Employee Profile Report")
	public void verify_report_header_as_Employee_Profile_Report() {
		maEmployeeProfileReportPage.validateEmployeeProfileReportName();
	}

	
	
}
