package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrFlexReportPage;


@SuppressWarnings("deprecation")
public class HrFlexReportPageSteps {
	
	HrFlexReportPage hrFlexReportPage;
	TestContext testContext;
	

	public HrFlexReportPageSteps(TestContext context) {
		testContext = context;
		hrFlexReportPage = testContext.getPageObjectManager().getHrFlexReportPage();
	}
	    //Flex report
		//changes by Arjun
		@When("user click dsosg flex point detailed report link")
		public void user_click_dsosg_flex_point_detailed_report_link() {
			hrFlexReportPage.clickEmployeeFlexReport();
			
		}
		//changes by Arjun
		@When("user click employee flex status as {string}")
		public void user_click_employee_flex_status_as(String string) {
			hrFlexReportPage.selectEmployeeFlexStatus();
			
		}
		//changes by Arjun
		@When("user click flex report search button")
		public void user_click_flex_report_search_button() {
			hrFlexReportPage.clickFlexReportSearchButton();
			
		}
		//changes by Arjun
		@Then("verify report header as {string}")
		public void verify_report_header_as(String string) {
			hrFlexReportPage.verifyFlexReportHeader();
			
		}
}