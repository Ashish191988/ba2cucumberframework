package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.ImplementationPage;
import pageObjects.InitializationPage;
import pageObjects.UploadEmpDepDataPage;

public class UploadEmpDepDataPageSteps {

	UploadEmpDepDataPage uploadEmpDepDataPage;
	ImplementationPage implementationPage;
	TestContext testContext;

	public UploadEmpDepDataPageSteps(TestContext context) {
		testContext = context;
		uploadEmpDepDataPage = testContext.getPageObjectManager().getUploadEmpDepDataPage();
		implementationPage = testContext.getPageObjectManager().getImplementationPage();
	}

	@Then("user upload the employee data")
	public void user_upload_the_employee_data() {
		implementationPage.clickUploadEmployeeDataButton();
		uploadEmpDepDataPage.uploadEmployeeCsv();
		uploadEmpDepDataPage.clickStartEmpUpload();
		uploadEmpDepDataPage.clickEmpSubmitButton();
		uploadEmpDepDataPage.clickCloseButton();
	}

	@Then("user upload the dependent data")
	public void user_upload_the_dependent_data() {
		implementationPage.clickUploadDepDataButton();
		uploadEmpDepDataPage.uploadDepCsv();
		uploadEmpDepDataPage.clickStartDepUpload();
		uploadEmpDepDataPage.clickDepSubmitButton();
		uploadEmpDepDataPage.clickCloseButton();

	}

	/*
	 * @Then("user upload the employee csv") public void
	 * user_upload_the_employee_csv() { uploadEmpDepDataPage.uploadEmployeeCsv();
	 * 
	 * }
	 * 
	 * @Then("user click on the start upload button") public void
	 * user_click_on_the_start_upload_button() {
	 * uploadEmpDepDataPage.clickStartEmpUpload(); }
	 * 
	 * @Then("user click on the submit button") public void
	 * user_click_on_the_submit_button() {
	 * uploadEmpDepDataPage.clickEmpSubmitButton(); }
	 * 
	 * @Then("user click on the close button") public void
	 * user_click_on_the_close_button() { uploadEmpDepDataPage.clickCloseButton(); }
	 * 
	 * @Then("user upload the dependent csv") public void
	 * user_upload_the_dependent_csv() { uploadEmpDepDataPage.uploadDepCsv(); }
	 * 
	 * @Then("user click on the dep start upload button") public void
	 * user_click_on_the_dep_start_upload_button() {
	 * uploadEmpDepDataPage.clickStartDepUpload(); }
	 * 
	 * @Then("user click on the dep submit button") public void
	 * user_click_on_the_dep_submit_button() {
	 * uploadEmpDepDataPage.clickDepSubmitButton(); }
	 */

}
