package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MaDependentProfileReportPage;

public class MaDependentProfileReportSteps {

	MaDependentProfileReportPage maDependentProfileReportPage;
	TestContext testContext;

	public MaDependentProfileReportSteps(TestContext context) {
		testContext = context;
		maDependentProfileReportPage = testContext.getPageObjectManager().getMaDependentReportsPage();
	}

	@When("user click on the Dependent Profile Report under Report Stat")
	public void user_click_on_the_Dependent_Profile_Report_under_Report_Stat() {
		maDependentProfileReportPage.clickDependentProfileReport();
	}
/*
	@When("user select employee status as {string}")
	public void user_select_employee_status_as(String string) {
		maDependentProfileReportPage.clickEmployeeStatusDd();
	}
*/
	/*@When("user click dependent search button")
	public void user_click_dependent_search_button() {
		maDependentProfileReportPage.clickDependentSearchButton();
	}
*/
	@Then("verify report header as Dependent Profile Report")
	public void verify_report_header_as_Dependent_Profile_Report() {
		maDependentProfileReportPage.validateDependentProfileReportName();
	}

}
