package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrEmployeeSearchPage;

@SuppressWarnings("deprecation")
public class HrEmployeeSearchPageSteps {
	
	HrEmployeeSearchPage hrEmployeeSearchPage;
	TestContext testContext;
	

	public HrEmployeeSearchPageSteps(TestContext context) {
		testContext = context;
		hrEmployeeSearchPage = testContext.getPageObjectManager().getHrEmployeeSearchPage();
	}		
	//Changes by Arjun
	@When("user click search employee tab")
	public void user_click_search_employee_tab() {
		hrEmployeeSearchPage.clickHrSearchEmployeeTab();
	}
	//Changes by Arjun
	@When("user select employee id radio button")
	public void user_select_employee_id_radio_button() {
		hrEmployeeSearchPage.clickHrEmployeeIdRadioButton();
	}
	//Changes by Arjun
	@When("user enter {string} in search text box")
	public void user_enter_in_search_text_box(String string) {
		hrEmployeeSearchPage.clickHrSearchEmployeeTab();
	}
	//Changes by Arjun
	@When("user click search button to search employee")
	public void user_click_search_button_to_search_employee() {
		hrEmployeeSearchPage.clickHrEmployeeSearchButton();
	}
	//Changes by Arjun
	@When("user click employee link")
	public void user_click_employee_link() {
		hrEmployeeSearchPage.clickHrEmployeeIdLink();
	}
	//Changes by Arjun
	@Then("verify header for Employee Detail page")
	public void verify_header_for_Employee_Detail_page() {
		hrEmployeeSearchPage.verifyHrEmployeeSearchHeader();
	}
	//Changes by Arjun
	@When("user click dependent link")
	public void user_click_dependent_link() {
		hrEmployeeSearchPage.clickHrDependentSearchLink();
	}
	
	//Changes by Arjun
	@Then("verify verify header for Dependent detail page")
	public void verify_verify_header_for_Dependent_detail_page() {
		hrEmployeeSearchPage.verifyHrDependentSearchHeader();
	}
}