package stepDefinations;


import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MaDependentProfileReportPage;
import pageObjects.MaPanelClaimListingReportPage;


public class MaPanelClaimListingReportSteps {

	
	MaPanelClaimListingReportPage maPanelClaimListingReportPage;
	TestContext testContext;
	

	public MaPanelClaimListingReportSteps(TestContext context) {
		testContext = context;
		maPanelClaimListingReportPage = testContext.getPageObjectManager().getMaPanelClaimListingReportPage();
	}
	


	@When("user click on the Panel Claim Listing Report under Report Stat")
	public void user_click_on_the_Panel_Claim_Listing_Report_under_Report_Stat() {
		maPanelClaimListingReportPage.clickPanelClaimListingReport();
	}

	/*@When("user select {string} in Status")
	public void user_select_in_Status(String string) {
		maPanelClaimListingReportPage.clickStatusDd();
	}
*/
	@When("user click Generate button")
	public void user_click_Generate_button() {
		maPanelClaimListingReportPage.clickGenerateButton();
	}

	@Then("verify report header as Panel Claim Listing Report")
	public void verify_report_header_as_Panel_Claim_Listing_Report() {
		maPanelClaimListingReportPage.validatePanelClaimListingReportName();
	}

}
