package stepDefinations;

//import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrEmployeeReportPage;
//import pageObjects.HrPage;
//import pageObjects.HrReportPage;


@SuppressWarnings("deprecation")
public class HrEmployeeReportPageSteps {
	
	HrEmployeeReportPage hrEmployeeReportPage;
	TestContext testContext;
	

	public HrEmployeeReportPageSteps(TestContext context) {
		testContext = context;
		hrEmployeeReportPage = testContext.getPageObjectManager().getHrEmployeeReportPage();
	}
	
	
	@Given("user login into EE application")
	public void user_login_into_EE_application() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}
    //Changed by Arjun
	@Given("user click enter button under admin section")
	public void user_click_enter_button_under_admin_section() {
		hrEmployeeReportPage.clickHrAdminButton();

	}
	//Changed by Arjun
	@When("user click reports tab")
	public void user_click_reports_tab() {
		hrEmployeeReportPage.clickHrReportTab();
	}
	//Changed by Arjun
	@When("user click employee profile report link")
	public void user_click_employee_profile_report_link() {
		hrEmployeeReportPage.clickEmployeeProfileRepot();
	}
	//Changed by Arjun
	@When("user click employee status as {string}")
	public void user_click_employee_status_as(String string) {
		hrEmployeeReportPage.selectEmployeeStatus();
	}
	//Changed by Arjun
	@When("user click employee search button")
	public void user_click_employee_search_button() {
		hrEmployeeReportPage.clickSearchEmployeeReportButton();
	}
	
	//Changed by Arjun
	@Then("verify employee report header as {string}")
	public void verify_employee_report_header_as(String string) {
		hrEmployeeReportPage.verifyEmployeeReportHeader();
	}
	
}