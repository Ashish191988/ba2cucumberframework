package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrClaimSearchPage;

@SuppressWarnings("deprecation")
public class HrClaimSearchPageSteps {
	
	HrClaimSearchPage hrClaimSearchPage;
	TestContext testContext;
	

	public HrClaimSearchPageSteps(TestContext context) {
		testContext = context;
		hrClaimSearchPage = testContext.getPageObjectManager().getHrClaimSearchPage();
	}		

		//Changes by Arjun
		@When("user click claims tab")
		public void user_click_claims_tab() {
			hrClaimSearchPage.clickHrClaimTab();
		}

		//Changes by Arjun
		@When("user enter {string} in claim search text box")
		public void user_enter_in_claim_search_text_box(String string) {
			hrClaimSearchPage.selectHrClaimStatus();
		}

		//Changes by Arjun
		@When("user click on search button on search claim page")
		public void user_click_on_search_button_on_search_claim_page() {
			hrClaimSearchPage.clickHrClaimSearchButton();
		}

		//Changes by Arjun
		@Then("verify claim tables header is displayed as Transaction Code")
		public void verify_claim_tables_header_is_displayed_as_Transaction_Code() {
			hrClaimSearchPage.verifyHrClaimTransactionCodeHeader();
		}

		
		
		
	}
