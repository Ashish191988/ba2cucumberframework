/*package stepDefinations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrPage;
import pageObjects.ImplementationPage;

@SuppressWarnings("deprecation")
public class HrPageSteps {
	
	HrPage hrPage;
	ImplementationPage implementationPage;
	TestContext testContext;
	

	public HrPageSteps(TestContext context) {
		testContext = context;
		implementationPage = testContext.getPageObjectManager().getImplementationPage();
		hrPage = testContext.getPageObjectManager().getHrPage();
	}
	
	//changes by Arjun
	@When("user click on security access link and click on manage user link")
	public void user_click_on_security_access_link_and_click_on_manage_user_link() {
		hrPage.clickManageUserButton();
	}

	//changes by Arjun
	@And("user click on add client admin button")
	public void user_click_on_add_client_admin_button() {
	    hrPage.clickAddClientAdminButton();
	}

	//changes by Arjun
	@And("user click on search for employee button")
	public void user_click_on_search_for_employee_button() {
		hrPage.clickSearchForEmployeeButton();
	}

	//changes by Arjun
	@And("user enter employee name as {string} and click on search button")
	public void user_enter_employee_name_as_and_click_on_search_button(String string) {
	   hrPage.enterEmployeeFullName();
	   hrPage.clickSearchEnteredEmployeeButton();
	}

	//changes by Arjun
	@And("user click on employee name link")
	public void user_click_on_employee_name_link() {
	    hrPage.clickSearchedEmployeeLinkButton();
	}

	//changes by Arjun
	@Then("user click on save button")
	public void user_click_on_save_button() {
	   hrPage.clickFinalSavebutttonButton();
	}


	
}*/
	