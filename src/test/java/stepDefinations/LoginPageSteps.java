package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.LoginPage;

public class LoginPageSteps {

	LoginPage loginPage;
	TestContext testContext;

	public LoginPageSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}

	@Given("user login into the application")
	public void user_login_into_the_application() {
		System.out.println("User launch the browser");
		loginPage.enterEmailAddress();
		loginPage.clickNextButton();
	}
	
	@Then("user logout from the application")
	public void user_logout_from_the_application() {
		loginPage.clickLogOut();
	}
	
	


}
