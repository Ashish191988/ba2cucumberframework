package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrSoeSearchPage;

@SuppressWarnings("deprecation")
public class HrSoeSearchPageSteps {
	
	HrSoeSearchPage hrSoeSearchPage;
	TestContext testContext;
	

	public HrSoeSearchPageSteps(TestContext context) {
		testContext = context;
		hrSoeSearchPage = testContext.getPageObjectManager().getHrSoeSearchPage();
		
	}		

	
	
	//Changes by Arjun
	@When("user click stataement of account link")
	public void user_click_stataement_of_account_link() {
		hrSoeSearchPage.clickHrSoaSearchLink();
	}
	//Changes by Arjun
	@Then("verify header for stetement of account page")
	public void verify_header_for_stetement_of_account_page() {
		hrSoeSearchPage.verifyHrSoaHeader();
	}

	//Changes by Arjun
	@When("user click start enrollment tab")
	public void user_click_start_enrollment_tab() {
		hrSoeSearchPage.clickHrEnrolmentTab();
	}

	//Changes by Arjun
	@When("user enter {string} in enrolment search text box")
	public void user_enter_in_enrolment_search_text_box(String string) {
		hrSoeSearchPage.clickHrEmployeeEnrolmentTextBox();
	}

	//Changes by Arjun
	@When("user click search button to search SOE")
	public void user_click_search_button_to_search_SOE() {
		hrSoeSearchPage.clickHrEnrolmentSearchButton();
	}

	//Changes by Arjun
	@Then("verify employees SOE header as employee")
	public void verify_employees_SOE_header_as_employee() {
		hrSoeSearchPage.verifyHrEnrolmentHeader();
	}
}