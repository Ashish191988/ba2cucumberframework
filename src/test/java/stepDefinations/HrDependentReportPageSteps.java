package stepDefinations;

//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.HrDependentReportPage;


@SuppressWarnings("deprecation")
public class HrDependentReportPageSteps {
	
	HrDependentReportPage hrDependentReportPage;
	TestContext testContext;
	

	public HrDependentReportPageSteps(TestContext context) {
		testContext = context;
		hrDependentReportPage = testContext.getPageObjectManager().getHrDependentReportPage();
	}
	    //Changes by Arjun 
		//Dependent Report
		@When("user click dependent profile report link")
		public void user_click_dependent_profile_report_link() {
			hrDependentReportPage.clickDependentProfileReport();
		}
		//Changes by Arjun
		@When("user click dependent status as {string}")
		public void user_click_dependent_status_as(String string) {
			hrDependentReportPage.selectDependentStatus();
		}
		//Changes by Arjun
		@When("user click dependent search button")
		public void user_click_dependent_search_button() {
			hrDependentReportPage.clickSearchDependentReportButton();
		}
		//Changes by Arjun
		@Then("verify  dependent report header as {string}")
		public void verify_dependent_report_header_as(String string) {
		    hrDependentReportPage.verifyDependentReportHeader();
		}
}