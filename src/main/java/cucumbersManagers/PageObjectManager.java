package cucumbersManagers;

import org.openqa.selenium.WebDriver;

import pageObjects.EeSubmitClaimCommonPage;
import pageObjects.HrClaimSearchPage;
import pageObjects.HrDependentReportPage;
import pageObjects.HrEmployeeReportPage;
import pageObjects.HrFlexReportPage;
import pageObjects.HrPage;
import pageObjects.HrSoeSearchPage;
import pageObjects.ImplementationPage;
import pageObjects.InitializationPage;
import pageObjects.LoginPage;
import pageObjects.MaDependentProfileReportPage;
import pageObjects.MaEmployeeProfileReportPage;
import pageObjects.MaPanelClaimListingReportPage;
import pageObjects.UploadEmpDepDataPage;
import pageObjects.HrEmployeeSearchPage;

public class PageObjectManager {

	private WebDriver driver;

	private LoginPage loginPage;

	private ImplementationPage implementationPage;

	private InitializationPage initializationPage;

	private UploadEmpDepDataPage uploadEmpDepDataPage;
	
	//Changes by Mohit
	private MaEmployeeProfileReportPage maReportsPage;
    
	//Changes by Mohit
    private MaDependentProfileReportPage maDependentReportsPage;
    
    //Changes by Mohit
    private MaPanelClaimListingReportPage maPanelClaimListingReportPage;

    //Changes by Arjun
    private HrPage hrPage;
	    
	//Changes by Arjun
    private HrEmployeeReportPage hrEmployeeReportPage;
	
    //Changes by Arjun
	private HrDependentReportPage hrDependentReportPage;

	//Changes by Arjun
	private HrFlexReportPage hrFlexReportPage;
		
	//Changes by Arjun
	private HrEmployeeSearchPage hrEmployeeSearchPage;
				
	//Changes by Arjun
	private HrSoeSearchPage hrSoeSearchPage;
				
	//Changes by Arjun
	private HrClaimSearchPage hrClaimSearchPage;
		
	//Changes by Arjun
	private EeSubmitClaimCommonPage eeSubmitChildcareServicesClaimPage;

	// Constructor
	public PageObjectManager(WebDriver driver) {

		this.driver = driver;

	}

	public UploadEmpDepDataPage getUploadEmpDepDataPage() {

		return (uploadEmpDepDataPage == null) ? uploadEmpDepDataPage = new UploadEmpDepDataPage(driver) : uploadEmpDepDataPage;

	}

	public LoginPage getLoginPage() {

		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;

	}

	public ImplementationPage getImplementationPage() {

        return (implementationPage == null) ? implementationPage = new ImplementationPage(driver) : implementationPage;

    } 


	public InitializationPage getInitializationPage() {

		return (initializationPage == null) ? initializationPage = new InitializationPage(driver) : initializationPage;

	}

	//Changes by Arjun
	
	
	public HrPage getHrPage() {

		return (hrPage == null) ? hrPage = new HrPage(driver) : hrPage;

	}
	
	//changes by Arjun
	
	public HrEmployeeReportPage getHrEmployeeReportPage() {

		return (hrEmployeeReportPage == null) ? hrEmployeeReportPage = new HrEmployeeReportPage(driver) : hrEmployeeReportPage;

	}
	
    //changes by Arjun
	public HrDependentReportPage getHrDependentReportPage() {

		return (hrDependentReportPage == null) ? hrDependentReportPage = new HrDependentReportPage(driver) : hrDependentReportPage;

	}
	
    //changes by Arjun
	public HrFlexReportPage getHrFlexReportPage() {

		return (hrFlexReportPage == null) ? hrFlexReportPage = new HrFlexReportPage(driver) : hrFlexReportPage;

	}
	
	//changes by Arjun
		public HrEmployeeSearchPage getHrEmployeeSearchPage() {

			return (hrEmployeeSearchPage == null) ? hrEmployeeSearchPage = new HrEmployeeSearchPage(driver) : hrEmployeeSearchPage;

		}
		
		//changes by Arjun
		public HrSoeSearchPage getHrSoeSearchPage() {

			return (hrSoeSearchPage == null) ? hrSoeSearchPage = new HrSoeSearchPage(driver) : hrSoeSearchPage;

		}
		
		//changes by Arjun
		public HrClaimSearchPage getHrClaimSearchPage() {

			return (hrClaimSearchPage == null) ? hrClaimSearchPage = new HrClaimSearchPage(driver) : hrClaimSearchPage;

		}
	
	////Changes by Mohit
	public MaEmployeeProfileReportPage getMaReportsPage() {

        return (maReportsPage == null) ? maReportsPage = new MaEmployeeProfileReportPage(driver) : maReportsPage;

    }
 
	//Changes by Mohit
    public MaDependentProfileReportPage getMaDependentReportsPage() {

        return (maDependentReportsPage == null) ? maDependentReportsPage = new MaDependentProfileReportPage(driver) : maDependentReportsPage;

    }
  
    //Changes by Mohit
    public MaPanelClaimListingReportPage getMaPanelClaimListingReportPage() {

        return (maPanelClaimListingReportPage == null) ? maPanelClaimListingReportPage = new MaPanelClaimListingReportPage(driver) : maPanelClaimListingReportPage;

    }
 
   

	public EeSubmitClaimCommonPage getEeSubmitClaimCommonPage() {
		// TODO Auto-generated method stub
		return (eeSubmitChildcareServicesClaimPage == null) ? eeSubmitChildcareServicesClaimPage = new EeSubmitClaimCommonPage(driver) : eeSubmitChildcareServicesClaimPage;
	}
}
