package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class UploadEmpDepDataPage extends Base {

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_fupdDataFile')]")
	public WebElement BrowseFile;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_EmployeeDataNew_btnStartUpload')]")
	public WebElement StartEmpUpload;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_EmployeeDataNew_btnSubmit')]")
	public WebElement EmpSubmitButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_DependentDataNew_btnSubmitDep')]")
	public WebElement DepSubmitButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_btnClose')]")
	public WebElement CloseButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_btnStartUpload')]")
	public WebElement StartDepUpload;

	// Constructor
	public UploadEmpDepDataPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickCloseButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(CloseButton));
		clickButton(CloseButton);
		staticwait(3);
	}

	public void uploadEmployeeCsv() {
		// executeInputOperation(BrowseFile, System.getProperty("user.dir") +
		// "\\TestData\\emp.csv");
		BrowseFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\emp.csv");
	}

	public void uploadDepCsv() {
		// executeInputOperation(BrowseFile, System.getProperty("user.dir") +
		// "\\TestData\\emp.csv");
		BrowseFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\dep.csv");
	}

	public void clickStartEmpUpload() {
		clickButton(StartEmpUpload);
	}

	public void clickStartDepUpload() {
		clickButton(StartDepUpload);
	}

	public void clickEmpSubmitButton() {
		clickButton(EmpSubmitButton);
	}

	public void clickDepSubmitButton() {
		clickButton(DepSubmitButton);
	}

}
