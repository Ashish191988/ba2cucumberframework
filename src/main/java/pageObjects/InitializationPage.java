package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class InitializationPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	Xls_Reader_Fillo filloReader = null;

	// Page Factory- OR:
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtEnrolStDt')]")
	public WebElement enrolmentFrom;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtEnrolEndDt')]")
	public WebElement enrolmentTo;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtHREmployeeId')]")
	public WebElement employeeName;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_btnSubmit')]")
	public WebElement searchEmployee;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_txtSearch')]")
	public WebElement SearchEmployeeNew_txtSearch;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_rdbsearch_2')]")
	public WebElement SearchEmployeeNew_rdbsearch;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_btnSearch')]")
	public WebElement SearchEmployeeNew_btnSearch;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_SearchEmployeeNew_ddlPageSize')]")
	public WebElement selectNoOfRecords;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_btnSubmit')]")
	public WebElement SearchEmployeeNew_btnSubmit;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_ucScheduleJob_btnOk')]")
	public WebElement ScheduleJob_btnOk;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_BA2JobProgress1_btnClose')]")
	public WebElement JobProgress1_btnClose;

	@FindBy(how = How.XPATH, using = "//*[Contains(@id, '_ChoosePlanNew_BA2JobProgress1_btnClose')]")
	public WebElement ChoosePlanNew_BA2JobProgress1_btnClose;

	// Constructor
	public InitializationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void enterEmrolmentStartDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrolmentFrom, filloReader.getCellData("Select * from testData", "Enrolment From"));
	}

	public void enterEnrolmentEndDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrolmentTo, filloReader.getCellData("Select * from testData", "Enrolment To"));
	}

	public void enterEmployeeName() {
		enterText(employeeName, "BA41");
	}

	public void searchEmployeeButton() {
		clickButton(searchEmployee);
	}

	public void enterEmployeeNameInSearchField() {
		enterText(SearchEmployeeNew_txtSearch, "BA41");
	}

	public void clickEmployeeRadioButton() {
		clickButton(SearchEmployeeNew_rdbsearch);
	}

	public void clickSubmitButton() {
		clickButton(SearchEmployeeNew_btnSearch);
	}

	public void selectNoOFRecords() {
		executeInputOperation(selectNoOfRecords, "100");
	}

	public void clickSubmitButtonForInitialization() {
		clickButton(SearchEmployeeNew_btnSubmit);
	}

	public void clickScheduleJob_btnOk() {
		clickButton(ScheduleJob_btnOk);
	}

	public void clickJobProgress1_btnClose() {
		clickButton(JobProgress1_btnClose);
	}

}
