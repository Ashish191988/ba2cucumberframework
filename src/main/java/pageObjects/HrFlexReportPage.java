package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HrFlexReportPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;
	

			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//a[contains(text(),'Flex Balance And Usage Report')]")
	        public WebElement employeeFlexReport;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//select[contains(@id, '_RptFlexUsuage_lstddlStatus')]")
			public WebElement employeeFlexStatus;
					
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptFlexUsuage_btnShowReport')]")
			public WebElement flexReportSearch;
					
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//div[contains(text(),'Flex Balance & Usage Report')]")
			public WebElement flexReportHeader;
			
			// Constructor
			public HrFlexReportPage(WebDriver driver) {
				this.driver = driver;
				PageFactory.initElements(driver, this);
			}
			//Changes by Arjun for flex report
			public void clickEmployeeFlexReport() {
				clickButton(employeeFlexReport);
				
			}

			// changes By Arjun for dropdown
			public void selectEmployeeFlexStatus() {
				SelectFromListBox(employeeFlexStatus, "Active");
			
			}

	        //Changes by Arjun
			public void clickFlexReportSearchButton() {
				clickButton(flexReportSearch);
			}
			
			//Changes by Arjun need to verify 
			public void verifyFlexReportHeader() {
				enterText(flexReportHeader, "Flex Balance & Usage Report");
			}
			
}