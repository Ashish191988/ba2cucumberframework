package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;
import pageObjects.LoginPage;

public class HrEmployeeSearchPage extends Base {
	
	
	//Changes by Arjun
		public static Logger log = LogManager.getLogger(LoginPage.class.getName());
		
		//Changes by Arjun
		WebDriver driver;

		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//span[contains(text(),'Search Employee')]")
        public WebElement hrSearchEmployeeTab;
	
	    //changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_rdbsearch_2')]")
        public WebElement hrEmployeeIdRadioButton;
		
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_txtSearch')]")
		public WebElement hrEmployeeTextBox;
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_btnSearch')]")
		public WebElement hrEmployeeSearchButton;
		
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'BA2_DSO_089')]")
		public WebElement hrEmployeeIdLink;
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Personal Details')]")
		public WebElement hrEmployeeSearchHeader;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'Dependents')]")
		public WebElement hrDependentSearchLink;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Dependents')]")
		public WebElement hrDependentSearchHeader;
				
		
		// Constructor
		public HrEmployeeSearchPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}

		//Changes by Arjun
		public void clickHrSearchEmployeeTab() {
			clickButton(hrSearchEmployeeTab);
		}
	
		//Changes by Arjun need to check
		public void clickHrEmployeeIdRadioButton() {
			RadioSelector(hrEmployeeIdRadioButton, "Select");
			
		}
		
		//Changes by Arjun need to check got text
		public void clickHrSearchEmployeeTab1() {
			clickButton(hrSearchEmployeeTab);
		}
		
		//Changes by Arjun
		public void clickHrEmployeeSearchButton() {
			clickButton(hrEmployeeSearchButton);
		}
		
		//Changes by Arjun
		public void clickHrEmployeeIdLink() {
			clickJS(hrEmployeeIdLink);
		}
		
		//Changes by Arjun
		public void verifyHrEmployeeSearchHeader() {
			validateTextfieldvalue(hrEmployeeSearchHeader, "Employee Detail page");
			
		}
		
		//Changes by Arjun
		public void clickHrDependentSearchLink() {
			clickJS(hrDependentSearchLink);
		}
		
		//Changes by Arjun need to verify 
		public void verifyHrDependentSearchHeader() {
			validateTextfieldvalue(hrDependentSearchHeader, "Dependent detail page");
			
		}
}
