package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HrSoeSearchPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;

		//changes by Arjun
	    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Statement Of Account')]")
        public WebElement hrSoaSearchLink;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Statement of Account')]")
        public WebElement hrSoaHeader;
		
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//span[contains(text(),'Start Enrollment')]")
		public WebElement hrEnrolmentTab;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ViewEmployeeEnrolmentStatement_SearchEnrolmentStatus_v2_txtSearch')]")
		public WebElement hrEmployeeEnrolmentTextBox;
	
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ViewEmployeeEnrolmentStatement_SearchEnrolmentStatus_v2_btnSearch')]")
		public WebElement hrEnrolmentSearchButton;
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//h2[contains(text(),'Employees')]")
		public WebElement hrEnrolmentHeader;
				
		// Constructor
		public HrSoeSearchPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		//Changes by Arjun
		public void clickHrSoaSearchLink() {
			clickButton(hrSoaSearchLink);
		}
		
		//Changes by Arjun need to verify 
		public void verifyHrSoaHeader() {
			validateTextfieldvalue(hrSoaHeader, "Statement of Account");
			
	    }
		
		//SOE Search
		//Changes by Arjun
		public void clickHrEnrolmentTab() {
			clickButton(hrEnrolmentTab);
		}

		//Changes by Arjun
		public void clickHrEmployeeEnrolmentTextBox() {
			clickButton(hrEmployeeEnrolmentTextBox);
		}
		
		
		//Changes by Arjun
		public void clickHrEnrolmentSearchButton() {
			clickButton(hrEnrolmentSearchButton);
		}
		
		
		//Changes by Arjun  
		public void verifyHrEnrolmentHeader() {
			validateTextfieldvalue(hrEnrolmentHeader, "Employees");
			
		}
}