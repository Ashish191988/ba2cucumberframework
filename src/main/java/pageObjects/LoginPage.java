package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class LoginPage extends Base {
	
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");

	String email = filloReader.getCellData("Select * from testData", "Email");

	// Page Factory- OR:
	@FindBy(how = How.ID, using = "ContentPlaceHolder1_TextBox1")
	//@CacheLookup
	public WebElement emailAddress;

	@FindBy(how = How.ID, using = "ContentPlaceHolder1_PassiveSignInButton")
	//@CacheLookup
	public WebElement nextButton;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@id,'_dnnLOGIN_cmdLogin')]")
	public WebElement logOut;
	

	// Constructor
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void enterEmailAddress() {
		//System.out.println(email);
		executeInputOperation(emailAddress, email);
		log.info("Enter the email address" + email  +  " for the employee in the " + emailAddress + "Field");

	}

	public void clickNextButton() {
		log.info("Clicked on the " + nextButton.getText());
		clickButton(nextButton);
		
	}
	
	public void clickLogOut() {
		log.info("Clicking on logout button");
		clickButton(logOut);
		log.info("Clicked on logout button");
		
	}
	
	
}
