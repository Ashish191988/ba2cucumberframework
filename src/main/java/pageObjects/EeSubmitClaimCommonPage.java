package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;
//import cucumbersManagers.EeSubmitChildcareServicesClaimPage;

public class EeSubmitClaimCommonPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;
	
	//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnEmployee')]")
	        public WebElement eeEmployeeButton;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//a[contains(@id, '_BA2DashBoard_SUBMITCLAIM')]")
	        public WebElement createClaimLink;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_lstClaimantName')]")
	        public WebElement claimantNameDropDown;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_lstClaimItemName')]")
	        public WebElement claimTypeDropdown;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtProviderName')]")
	        public WebElement clinicProviderName;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptDate')]")
	        public WebElement receiptDate;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptNo')]")
	        public WebElement receiptNo;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptAmount')]")
	        public WebElement receiptAmount;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//textarea[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtEmployeeRemarks')]")
	        public WebElement remarkTextBox;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_File1')]")
	        public WebElement attachAttachment;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_EditUploadedFiles_grdFiles_ctl02_txtRemarks')]")
	        public WebElement attachAttachmentTextBox;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id. '_ClaimsSubmit_ClaimSubmitControlV2_ready')]")
	        public WebElement readyToSubmitButton;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmissionListV2_btnConfirmClaims')]")
	        public WebElement confirmSubmitButton;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//input[contains(@id, 'confirmButton')]")
	        public WebElement claimConfirmButton;
			
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//span[contains(@id, '_ClaimsSearch_lblTitle')]")
	        public WebElement eeClaimSearchHeader;
			
			// Constructor
			public EeSubmitClaimCommonPage(WebDriver driver) {
				this.driver = driver;
				PageFactory.initElements(driver, this);
			}
			
			//Changes by Arjun
			public void clickEeEmployeeButton() {
				clickButton(eeEmployeeButton);
			}
			
			//Changes by Arjun
			public void clickCreateClaimLink() {
				clickJS(createClaimLink);
			}
			
			//Changes by Arjun
			public void selectClaimantNameDropDown() { 
			       SelectFromListBox(claimantNameDropDown, " ");
			
			       /*
			        * List<List<String>> object = data.raw();
		Thread.sleep(2000);
		Assert.assertTrue(mmbHomePage.helpLinkText().getText().contains(object.get(0).get(0)));
		Assert.assertTrue(mmbHomePage.clientLegalLinkText().getText().contains(object.get(0).get(1)));
			        */
			       
			}
			
			//Changes by Arjun
			public void selectClaimTypeDropdown() { 
			       SelectFromListBox(claimTypeDropdown, "   Childcare services including infant care services (licensed centres only)");
			
			}
			
			//Changes by Arjun
			public void enterClinicProviderName() { 
			        enterText(clinicProviderName, "<clinic_provider_name>");
			}
			
			//Changes by Arjun
			public void enterReceiptDate() { 
		        enterText(receiptDate, "<receipt_date>");
		    }
			
			//Changes by Arjun
			public void enterReceiptNo() { 
		        enterText(receiptNo, "<receipt_no>");
		    }
			//Changes by Arjun
			public void enterReceiptAmount() { 
		        enterText(receiptAmount, "<receipt_amount>");
		    }
			
			//Changes by Arjun
			public void enterRemarkTextBox() { 
		        enterText(remarkTextBox, "<remark>");
		    }
			
			//Changes by Arjun need to verify
		 	public void enterAttachAttachment() { 
		        enterText(attachAttachment, "<clinic_provider_name>");
		    }
			
		 	//Changes by Arjun
			public void enterattachAttachmentTextBox() { 
		        enterText(receiptDate, "Test");
		    }
		 	
			//Changes by Arjun
			public void clickReadyToSubmitButton() {
				clickButton(readyToSubmitButton);
			}
			
			//Changes by Arjun
			public void clickConfirmSubmitButton() {
				clickButton(confirmSubmitButton);
			}
			
			//Changes by Arjun
			public void clickClaimConfirmButton() {
				clickButton(claimConfirmButton);
			}
			
			//Changes by Arjun
			public void verifyEeClaimSearchHeader() {
				validateTextfieldvalue(eeClaimSearchHeader, "Search Claim");
			}
}