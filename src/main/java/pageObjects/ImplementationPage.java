package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class ImplementationPage extends Base {

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Implementation')]")
	private WebElement Implementation;

	@FindBy(how = How.LINK_TEXT, using = "Upload Employee Data (New)")
	private WebElement UploadEmployeeData;

	@FindBy(how = How.LINK_TEXT, using = "Upload Dependent Data (New)")
	private WebElement UploadDepData;

	@FindBy(how = How.LINK_TEXT, using = "Initialization (New)")
	private WebElement Initialization;
	
	// Constructor
		public ImplementationPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
	

	public void clickUploadEmployeeDataButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, UploadEmployeeData);
	}

	public void clickInitializationButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, Initialization);
	}

	public void clickUploadDepDataButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, UploadDepData);
	}
    
	

	
}
