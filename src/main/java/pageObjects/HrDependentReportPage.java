package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HrDependentReportPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	        //Changes by Arjun
	        WebDriver driver;
	
	        //changes by Arjun
			@FindBy(how = How.XPATH, using = "//a[contains(text(),'Dependent Profile Report')]")
			public WebElement dependentProfileReport;
					
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//select[contains(@id, '_RptDependentProfile_lstddlStatus')]")
			public WebElement dependentStatus;
					
			//changes by Arjun
		    @FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptDependentProfile_btnShowReport')]")
	        public WebElement searchDependentReport;
					
			//changes by Arjun
			@FindBy(how = How.XPATH, using = "//div[contains(text(),'Dependent Profile Report')]")
	        public WebElement dependentReportHeader;
			
			// Constructor
			public HrDependentReportPage(WebDriver driver) {
				this.driver = driver;
				PageFactory.initElements(driver, this);
			}
			//Changes by Arjun for dependent profile
			public void clickDependentProfileReport() {
				clickJS(dependentProfileReport);
				
			}

			// changes By Arjun for dropdown
			public void selectDependentStatus() {
				SelectFromListBox(dependentStatus, "Active");
			
			}

		    //Changes by Arjun
			public void clickSearchDependentReportButton() {
				clickButton(searchDependentReport);
			}
			
			//Changes by Arjun need to verify 
			public void verifyDependentReportHeader() {
			        enterText(dependentReportHeader, "Dependent Profile Report");
			
			}
			
}