package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HrEmployeeReportPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;

	
	
	    //changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnAdmin')]")
        public WebElement hrAdminButton;
	
	    //changes by Arjun
		@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reports')]")
        public WebElement hrReportTab;
		
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'Employee Profile Report')]")
		public WebElement employeeProfileRepot;
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//select[contains(@id,'_RptEmployeeProfile_MY_lstddlStatus')]")
		public WebElement employeeStatus;
		
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptEmployeeProfile_MY_btnShowReport')]")
		public WebElement searchEmployeeReport;
		
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//div[contains(text(),'Employee Profile Report')]")
		public WebElement employeeReportHeader;
		
		// Constructor
		public HrEmployeeReportPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		
		//Changes by Arjun
		public void clickHrAdminButton() {
			clickButton(hrAdminButton);
		}
	
		//Changes by Arjun
		public void clickHrReportTab() {
			clickButton(hrReportTab);
		}
		
		//Changes by Arjun
		public void clickEmployeeProfileRepot() {
			clickJS(employeeProfileRepot);
			
		}
		
		// changes By Arjun for dropdown
		
		public void selectEmployeeStatus() {
			SelectFromListBox(employeeStatus, "Active");
		
		}

	
		//Changes by Arjun
		public void clickSearchEmployeeReportButton() {
			clickButton(searchEmployeeReport);
		}
		
		//Changes by Arjun for employee header 
		public void verifyEmployeeReportHeader() {
		    enterText(employeeReportHeader, "Employee Profile Report");
		
		}
		
}