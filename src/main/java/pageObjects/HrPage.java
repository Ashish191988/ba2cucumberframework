package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HrPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;

	
	//changes by Arjun
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'SecurityAccess')]")
		public WebElement SecurityAccess;

		//changes by Arjun
		@FindBy(how = How.LINK_TEXT, using = "Manage User")
		public WebElement ManageUser;
			

	    //changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserSearch_btnAddNewClientAdmin')]")
		public WebElement addClientAdmin;
			
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_UserCreateClientAdminExisting_btnSearchForEmployee')]")
		public WebElement searchForEmployee;
			
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_txtFullName')]")
		public WebElement employeeFullName;
			
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_btnSearch')and(@value='Search')]")
		public WebElement searchEnteredEmployee;
			
		//changes by Arjun need to check
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'BA2_DSO_037')]")
		public WebElement searchedEmployeeLink;
			
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_btnSave')]")
		public WebElement finalSavebuttton;
			
		// Constructor
		public HrPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		//Changes By Arjun
		public void clickManageUserButton() {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(SecurityAccess));
			clickActionClass(SecurityAccess, ManageUser);
		}
	
		//Changes by Arjun
		public void clickAddClientAdminButton() {
			clickButton(addClientAdmin);
		}
		
		//Changes by Arjun
		public void clickSearchForEmployeeButton() {
			clickButton(searchForEmployee);
		}
	
		//Changes by Arjun
		public void enterEmployeeFullName() {
			
			enterText(employeeFullName, "BA2_DSO_037");
		}
	
		//Changes by Arjun
	    public void clickSearchEnteredEmployeeButton() {
			clickButton(searchEnteredEmployee);
		}
		
	   //Changes by Arjun
	    public void clickSearchedEmployeeLinkButton() {
			clickButton(searchedEmployeeLink);
		}
		
	   //Changes by Arjun
	  	public void clickFinalSavebutttonButton() {
	  		clickButton(finalSavebuttton);
	  	}
	    
}