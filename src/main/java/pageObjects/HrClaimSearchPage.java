package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;


public class HrClaimSearchPage extends Base {
	
	
	//Changes by Arjun
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	//Changes by Arjun
	WebDriver driver;

	    //changes by Arjun
		@FindBy(how = How.XPATH, using = "//span[contains(text(),'Claims')]")
		public WebElement hrClaimTab;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSearch_ddlStatusSearchNew')]")
		public WebElement hrClaimStatus;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSearch_btnSearchNew')]")
		public WebElement hrClaimSearchButton;
				
		//changes by Arjun
		@FindBy(how = How.XPATH, using = "//a[contains(text(),'Transaction Code')]")
		public WebElement hrClaimTransactionCodeHeader;
		
		// Constructor
				public HrClaimSearchPage(WebDriver driver) {
					this.driver = driver;
					PageFactory.initElements(driver, this);
				}
		
				
				//Changes by Arjun
				public void clickHrClaimTab() {
					clickButton(hrClaimTab);
				}
				
				
				// changes By Arjun for dropdown
				public void selectHrClaimStatus() {
					SelectFromListBox(hrClaimStatus, "Active");
				
				}
				
				//Changes by Arjun
				public void clickHrClaimSearchButton() {
					clickButton(hrClaimSearchButton);
				}
				
				//Changes by Arjun  
				public void verifyHrClaimTransactionCodeHeader() {
				    validateTextfieldvalue(hrClaimTransactionCodeHeader, "Transaction Code");
				
				}
}