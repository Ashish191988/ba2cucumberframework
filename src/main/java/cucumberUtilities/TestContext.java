package cucumberUtilities;

import cucumbersManagers.PageObjectManager;
import cucumbersManagers.WebDriverManager;

public class TestContext {

	public WebDriverManager webDriverManager;
	public PageObjectManager pageObjectManager;
	public ScenarioContext scenarioContext;

	public TestContext() {
		// webDriverManager = new WebDriverManager();
		// pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
		scenarioContext = new ScenarioContext();
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}

	public ScenarioContext getScenarioContext() {
		return scenarioContext;
	}

}
